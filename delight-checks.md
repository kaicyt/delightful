# A delightful checklist

Read [What are delightful lists?](delightful.md) if you are unfamiliar with the concept.

For any curated delightful list to be added to the top-level project, it must adhere to the criteria mentioned in on this page.

## List quality

> **Important**: You should strive to only include the _best_ resources on your delightful lists!

After all this is about collecing the true _gems :gem: of freedom_ not just any 'Hello World' projects or information pages with casual rants.

After you submit your candidate list, the [maintainers](README.md#maintainers) will validate the criteria for inclusion, as well as gauge the quality of your list entries. We know that quality is subjective and we try to be as fair as possible here.

When there are quality issues with the list we'll start a discussion on our issue tracker and (try to) notify you. In case of a continued dispute, we may - with your consent - put out a poll on the fediverse using the [@humanetech](https://mastodon.social/@humanetech) account. But no matter the outcome of that vote, the final choice of the maintainers is at all times the deciding factor.

## Inclusion criteria

- Your delightful list is set up as a git repository.
  - It does not matter where your list is hosted. Can be Gitlab, Gitea, Sourcehut, etc. But we prefer [Codeberg](https://codeberg.org) as the location.

- You repository is structured as follows:
  - Repository name is in small caps, words separated by dashed and starting with 'delightful', like so: `delightful-<list-topic>`.
  - Repository description is brief and starts with `A curated list of delightful <list-topic> <...>`.
  - The curated list is maintained on the main `README.md` markdown list of the repository.

- See [Creating a delightful list](delight-us.md) for requirements and options for structuring your `README.md`.

### delightful badge

- Your list _must have_ the [![delightful](https://codeberg.org/teaserbot-labs/delightful/media/branch/main/assets/delightful-badge.png)](https://codeberg.org/teaserbot-labs/delightful) badge with a link to this project. The badge location is:
  - Right after the title text, if you have a markdown header as title.
  - After the list logo, but before the 'Contents' section starts.

Badge markdown for PNG badge (note: there is also a SVG version):

```markdown
[![delightful](https://codeberg.org/teaserbot-labs/delightful/media/branch/main/assets/delightful-badge.png)](https://codeberg.org/teaserbot-labs/delightful)
```

## All criteria met?

Please check these criteria very well, in order to lower the burden of quality checks for [maintainers](README.md#maintainers) as much possible.

If you are ready, you can place a request for inclusion to the top-level project. There are three options for to add your candidate list. From best to worst these are:

1. [Best option] If you have an account on [Codeberg](https://codeberg.org), or want to create one, then you can fork this repository, make a change to the `README.md` and send us your Pull Request. In your PR:
    - The PR title is `Add delightful-<list-topic>`.
    - The PR description includes `URL` of the list and a short summary explaining why it should be added.
    - The `README.md` update in the appropriate format - a link _to the README.md of your list_ (!) - followed by (preferably fitting a single line) a description. The entry placed under the desired category, and added in alphabetical (!) order to existing entries.
    - The `delightful-contributors.md` update, if you want to be attributed and are not yet on the list.

2. Alternatively [create an issue](https://codeberg.org/teaserbot-labs/delightful/issues/new) in our tracker, add the `candidate` label and provide the same info as is described in option 1.

3. [Least best option] If you don't want to create a Codeberg account, then you could send a request via the Fediverse by sending a _public_ toot (please do not DM for this!) to [@humanetech@mastodon.social](https://mastodon.social/@humanetech). See option one on what to include, and also add the `#delightful` hashtag.
    - Your request may be missed in the deluge of notications. In that case don't be disappointed, and just try again or create that Codeberg account and choose option 1 ;)

## After inclusion

We welcome you, [**delightful contributor**](delightful-contributors.md), and thank you for your help to make the world a tiny bit better together with us!